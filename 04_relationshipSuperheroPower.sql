CREATE TABLE SuperheroPowerLink (
SuperheroId INT FOREIGN KEY References Superhero(Id) ON DELETE CASCADE,
PowerId INT FOREIGN KEY References Power(Id) ON DELETE CASCADE,
PRIMARY KEY(SuperheroId, PowerId)
)
# Superheroes - SSMS

## Description

A program which displays use of DDL and DML queries in SSMS, to create a server regarding Superheroes.

## Installation

Install SSMS 18 and Microsoft SQL Server.

Install the project: 
Clone: https://gitlab.com/nicolai_eng/superheroes-ssms.git
```
```
## Contributors

* [NKE @nicolai_eng](@nicolai_eng)
